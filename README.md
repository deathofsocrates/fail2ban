# Fail2Ban IP Blocker

## Google Takeout Generation
A takeout .mbox file should be created so that this tool can be run.

1. Create Fail2Ban archive from [Google Takeout](https://www.google.com/settings/takeout)
2. Click 'Select none'
3. Select 'Mail' by first selecting the 'X', then click the dropdown for mail and select 'Select labels'
4. Choose 'Select none', then re-select 'SISTA/Servers/Fail2Ban' and click 'Done', then 'Next'
5. Set .zip for 'File type' and set 'Add to Drive' for 'Delivery method'
6. Select 'Create archive'
7. Wait for email, then download takout and drop into your working directory.

## Download Google Takout

1. Download most recent [Takeout Downloads](https://www.google.com/settings/takeout/downloads)
2. Select 'Open in drive' and download .zip
3. Choose one of two options
    1. Ensure that the Google Takeout .zip is in your ~/Downloads directory and run the cleanup then updater script.
        1. Run the cleanup script inside this repository ```./cleanup.sh```
        2. Run updater script inside this repository ```./updater.sh```
    2. Manually
        1. Unzip archive: ```unzip takeout-YYYYMMDDXXXXXXXX.zip```
        2. Move .mbox file to top level directory and cleanup directory: ```mv Takeout/Mail/SISTA/Servers/Fail2Ban.mbox .``` and ```rm -rf takeout-YYYYMMDDXXXXXXXX.zip Takeout```

## Create total dump of all Fail2Ban blocks for all hosts

1. Generate dump for all Fail2ban hosts: ```python3 parseomatic.py Fail2Ban.mbox all-hosts```

## Generate Host Blocks for a specific machine
1. Create a Summary of Fail2Ban dumps of blocked IPs for specified host: ```python3 banhammer.py all-hosts tunnel1.summary -n tunnel1```
2. Generate rule-set for mass-blocking of IPs for IPTables firewall with syntax included ```python3 banhammer.py all-hosts tunnel1.f2b -n tunnel1 -i```

## Add newly generated rules to host firewall
1. Get the latest IPTables rule-set from specified host: ```scp root@tunnel1:/etc/default/iptables-rules ipt```
2. Create iptables-rules set with specified ports for the host: ```python3 upgrayedd.py tunnel1.f2b ipt iptables-rules -p 22```

All iptables-rules files, must include the parsing flags for updating to
work correctly.

Upgradd expects:
```shell

# allow responses from outbound connections

-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

1.  Fail2Ban Drops

...

#  allow ssh from any

-A INPUT -p tcp -m tcp --dport 22 -j ACCEPT