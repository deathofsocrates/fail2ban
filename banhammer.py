from operator import itemgetter
from copy import deepcopy
import collections
import itertools
import argparse
import sys

THRESHOLD_8 = 15
THRESHOLD_16 = 5
THRESHOLD_24 = 15

parser = argparse.ArgumentParser()
    
parser.add_argument("input", type = str, help = "input file to read, must be parseomatic format")
parser.add_argument("output", type = str, help = "creates host/cidr in 'output' file")
parser.add_argument("-n", type = str, help = "name of the specific host to produce rules for")
parser.add_argument("-i", help = "creates ipt rules file in 'output' REQUIRES -n option", action="store_true")
    
args = parser.parse_args()
    
input_file = args.input 
output_file = args.output

class Host:

    def __init__(self, hostname):

        self.__ip = {}
        self.__ip_keys = []
        self.__hostname = hostname
        self.__attacks = 0
        self.__ports = []
        
    @property
    def hostname(self):
        return self.__hostname
        
    @hostname.setter
    def hostname(self, value):
        self.__hostname = value    
    
    @property
    def ip(self):
        return self.__ip
        
    @ip.setter
    def ip(self, value):
        self.__ip = value
        
    @property
    def ip_keys(self):
        return self.__ip_keys
        
    @ip_keys.setter
    def ip_keys(self, value):
        self.__ip_keys = value
        
    @property
    def attacks(self):
        return self.__attacks
        
    @attacks.setter
    def attacks(self, value):
        self.__attacks = value  
        
    @property
    def ports(self):
        return self.__ports
        
    @ports.setter
    def ports(self, value):
        self.__ports = value
          
def cidr(host_object, output_file):

    octals = []
    ban_32 = collections.OrderedDict()
    ban_24 = collections.OrderedDict()
    ban_16 = collections.OrderedDict()
    ban_8 = collections.OrderedDict()
       
    for octal in host_object.ip_keys:
        octal = "%3s.%3s.%3s.%3s" % tuple(octal.split("."))
        octal = octal.split(".")
        octals.append(octal)
       
    octals_copy = deepcopy(octals)
    remove_list = deepcopy(octals)
    
    for octal in octals:
        found = False
        for copy in octals_copy:
            if (octal[0] == copy[0]):
                if (octal != copy):
                    found = True
                    break
        if not found:
            string_ip = ".".join(octal).replace(" ", "") + "/32"
            if string_ip not in ban_32:
                ban_32[string_ip] = [".".join(octal).replace(" ", "")]
            else:
                ban_32[string_ip].append(".".join(octal).replace(" ", ""))
            if octal in remove_list:
                remove_list.remove(octal)
                        
    octals_copy = deepcopy(remove_list)
    octals = deepcopy(remove_list)

    temp_list = []
    for octal in octals:
        for copy in octals_copy:
            if ((octal[0] == copy[0]) and (octal[1] == copy[1]) and (octal[2] == copy[2])):
                if (octal != copy):
                    temp_list.append(octal)
    for octal in temp_list:
        if octal in remove_list:
            remove_list.remove(octal)
        octal[-1] = "0"
        string_ip = ".".join(octal).replace(" ", "") + "/24"
        if string_ip not in ban_24:
            ban_24[string_ip] = [".".join(octal).replace(" ", "")]
        else:
            ban_24[string_ip].append(".".join(octal).replace(" ", ""))

    octals_copy = deepcopy(remove_list)
    octals = deepcopy(remove_list)  
            
    temp_list = []
    for octal in octals:
        for copy in octals_copy:
            if ((octal[0] == copy[0]) and (octal[1] == copy[1])):
                if (octal != copy):
                    temp_list.append(octal)
    for octal in temp_list:
        if octal in remove_list:
            remove_list.remove(octal)
        octal[-1] = "0"
        octal[-2] = "0"
        string_ip = ".".join(octal).replace(" ", "") + "/16"
        if string_ip not in ban_16:
            ban_16[string_ip] = [".".join(octal).replace(" ", "")]
        else:
            ban_16[string_ip].append(".".join(octal).replace(" ", ""))
            
    for octal in remove_list:
        item = deepcopy(octal)
        item[-3] = "0"
        item[-2] = "0"
        item[-1] = "0/8"
        string_ip = ".".join(item).replace(" ", "")
        if string_ip not in ban_8:
            ban_8[string_ip] = [".".join(octal).replace(" ", "")]
        else:
            ban_8[string_ip].append(".".join(octal).replace(" ", ""))
     
    remove_24_list = []
    new_16_dict = collections.OrderedDict()
    
    for key in ban_24:
        if len(ban_24[key]) >= THRESHOLD_24:
            remove_24_list.append(key)
            split_key = key.split(".")
            split_key[-2] = "0"
            new_key = ".".join(split_key)
            new_key = new_key.replace("/24", "/16")
            if new_key not in new_16_dict:
                new_16_dict[new_key] = ban_24[key]
            else:
                new_16_dict[new_key].append(ban_24[key])
                
    for item in remove_24_list:
        if item in ban_24:
            del ban_24[item]

    remove_16_list = []
    new_8_dict = collections.OrderedDict()
    
    for key in ban_16:
        if len(ban_16[key]) >= THRESHOLD_16:
            remove_16_list.append(key)
            split_key = key.split(".")
            split_key[-2] = "0"
            split_key[-3] = "0"
            new_key = ".".join(split_key)
            new_key = new_key.replace("/16", "/8")
            if new_key not in new_8_dict:
                new_8_dict[new_key] = ban_16[key]
            else:
                new_8_dict[new_key].append(ban_16[key])
                
    for item in remove_16_list:
        if item in ban_16:
            del ban_16[item] 

    remove_8_list = []
    new_32_dict = collections.OrderedDict()       
    
    for key in ban_8:
        if len(ban_8[key]) <= THRESHOLD_8:
            remove_8_list.append(key)
            for ip in ban_8[key]:
                if (ip + "/32") not in new_8_dict:
                    new_32_dict[(ip + "/32")] = ip
                else:
                    new_8_dict[(ip + "/32")].append(ip)  
                         
    for item in remove_8_list:
        if item in ban_8:
            del ban_8[item]
     
    #add upgrade/downgrade dicts to cidr dicts
    for cidr in new_32_dict:
        ban_32[cidr] = new_32_dict[cidr]
       
    for cidr in new_16_dict:
        ban_16[cidr] = new_16_dict[cidr]
        
    for cidr in new_8_dict:
        ban_8[cidr] = new_8_dict[cidr]
     
    remove_32 = []
    remove_24 = []
    remove_16 = []
    for cidr in ban_8:
        temp_cidr = cidr.split(".")
        for ip in ban_32:
            temp_ip = ip.split(".")
            if temp_cidr[0] == temp_ip[0]:
                remove_32.append(ip)
                
        for ip in ban_24:
            temp_ip = ip.split(".")
            if temp_cidr[0] == temp_ip[0]:
                remove_24.append(ip)
                
        for ip in ban_16:
            temp_ip = ip.split(".")
            if temp_cidr[0] == temp_ip[0]:
                remove_16.append(ip)
                
    for cidr in ban_16:
        temp_cidr = cidr.split(".")
        for ip in ban_32:
            temp_ip = ip.split(".")
            if ((temp_cidr[0] == temp_ip[0]) and (temp_cidr[1] == temp_ip[1])):
                remove_32.append(ip)
                
        for ip in ban_24:
            temp_ip = ip.split(".")
            if ((temp_cidr[0] == temp_ip[0]) and (temp_cidr[1] == temp_ip[1])):
                remove_24.append(ip)
                
    for cidr in ban_24:
        temp_cidr = cidr.split(".")
        for ip in ban_32:
            temp_ip = ip.split(".")
            if ((temp_cidr[0] == temp_ip[0]) and (temp_cidr[1] == temp_ip[1]) and (temp_cidr[2] == temp_ip[2])):
                remove_32.append(ip)
  
    for cidr in remove_32:
        if cidr in ban_32:
            del ban_32[cidr]
        
    for cidr in remove_24:
        if cidr in ban_24:
            del ban_24[cidr]
        
    for cidr in remove_16:
        if cidr in ban_16:
            del ban_16[cidr]
        
    ban_32 = cidr_sort(ban_32)
    ban_24 = cidr_sort(ban_24)
    ban_16 = cidr_sort(ban_16)
    ban_8 = cidr_sort(ban_8)
     
    if (not args.i):
     
        [output_file.write(ip + "\n") for ip in ban_32]
        [output_file.write(ip + "\n") for ip in ban_24]
        [output_file.write(ip + "\n") for ip in ban_16]
        [output_file.write(ip + "\n") for ip in ban_8]
        
    else:
    
        [output_file.write("-A INPUT -s " + ip + " -j DROP\n") for ip in ban_32]
        [output_file.write("-A INPUT -s " + ip + " -j DROP\n") for ip in ban_24]
        [output_file.write("-A INPUT -s " + ip + " -j DROP\n") for ip in ban_16]
        [output_file.write("-A INPUT -s " + ip + " -j DROP\n") for ip in ban_8]
    
def cidr_sort(passed_dict):
    
    cidr_list = list(passed_dict.keys())
    
    for i in range(len(cidr_list)):
        cidr_list[i] = "%3s.%3s.%3s.%3s" % tuple(cidr_list[i].split("."))
            
    cidr_list.sort()
        
    cidr_list = list(cidr_list for cidr_list,_ in itertools.groupby(cidr_list))
           
    cidr_list = sorted(cidr_list, key = itemgetter(0, 1, 2, 3))
        
    for i in range(len(cidr_list)):
        cidr_list[i] = cidr_list[i].replace(" ", "")
        
    return cidr_list
                
def main():

    if (args.i and not args.n):
        parser.print_help(file=None)
        sys.exit(1)
    
    f = open(input_file, 'r')
    
    host_list = []
    
    temp_host = None
    
    for line in f:
    
        split_line = line.strip().split()
        
        if len(split_line) >= 2:
        
            if split_line[0] == "HOSTNAME:":
                temp_host = Host(split_line[1])
                
            elif split_line[0] == "IPS:":
                ip_list = split_line[1].split(",")
                for ip in ip_list:
                    occur = ip.split(":")
                    temp_host.ip_keys.append(occur[0])
                    temp_host.ip[occur[0]] = occur[1]
                    
            elif split_line[0] == "ATTACKS:":
                temp_host.attacks = int(split_line[1])
                
            elif split_line[0] == "PORTS:":
                temp_host.ports = split_line[1].split(",")
            
        if split_line and split_line[0] == "END":
            host_list.append(temp_host)
            temp_host = None
    
        o = open(output_file, 'w')
            
    
        if (args.n):
            for host in host_list:
                if host.hostname == args.n and not args.i:
                    o.write("===================== " + host.hostname + " =====================\n")
                    
                    for ip in host.ip_keys:
                        o.write(ip + " " + str(host.ip[ip]).rjust(30 - len(ip)) + "\n")
                        
                    o.write("\n===================== " + host.hostname + " rules =====================\n")
                    
                    cidr(host, o)
                
                elif (host.hostname == args.n and args.i):
                        
                    cidr(host, o)
        
        else:
            count = 0
            for host in host_list:
                newline = "\n"
                if count == 0:
                    newline = ""
                    count += 1
                o.write(newline + "===================== " + host.hostname + " =====================\n")
                    
                for ip in host.ip_keys:
                    o.write(ip + " " + str(host.ip[ip]).rjust(30 - len(ip)) + "\n")
                    
                o.write("\n===================== " + host.hostname + " rules =====================\n")
                        
                cidr(host, o)
                    
        o.close()
    
if __name__ == '__main__':
    main()