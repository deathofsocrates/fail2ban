#!/bin/bash

################
# unzip Takout #
################
# Move from Dowloads
echo "Moving Google takeout into place."
mv ~/Downloads/takeout-201*.zip .
echo "Unzipping Takout and moving .mbox into place..."
unzip takeout-201*.zip -d . 
mv Takeout/Mail/SISTA/Servers/Fail2Ban.mbox .
rm -rf takeout-201*.zip Takeout
echo "Cleaning up files..."

###################
# Run Parseomatic # 
###################
echo "Running Parsomatic to generate rule-sets for all hosts..."
python3 parseomatic.py Fail2Ban.mbox all-hosts
echo "Parsomatic run complete."
#########
# Usage #
#########
usage() {

    echo "Enter the following options:"
    echo "No optional arguments will cause this script to run on tunnel1/2 by default"
    echo "-a: All SISTA Servers"
    echo "-h: This help menu"

}

# Execute getopt
ARGS=$(getopt -o a:h -l "all-hosts,help" -n "getopt.sh" -- "$@");

# #Bad arguments
# if [ $# -eq 0 ]; then
#         usage
#         exit 0
# fi

eval set -- "$ARGS";

# Default to hosts tunnel1 and tunnel2
hosts="tunnel1 tunnel2"

# Parse options
while true; do
    case "$1" in
        -a|--all-hosts)
            shift;
            if [ -n "$1" ]; then
                hosts="tunnel1 tunnel2 rose martha presto yertle roslin adama to-vm";
                shift;
            fi
        ;;
        -h|--help)
            shift;
            if [ -n "$1" ]; then
                usage
                exit 0;
            fi
            ;;
        --)
            shift;
            break;
        	;;
    esac
done

#
# Create all .f2b rule-sets
#
echo "Creating per-host rule-sets..."
for h in $hosts; do
	echo "Creating F2B rule-set for $h..."
	python3 banhammer.py all-hosts ${h}.f2b -n ${h} -i
done
echo "Generation complete"
#
# Copy files and run updater
#
echo "Updating IPTables rule-sets on all hosts..."
for h in $hosts; do
	echo "Creating folder structure and copying relevent scripts and F2B rules for ${h}..."
	ssh root@$h "mkdir -p /root/bin/ipt"
	scp blocker.sh upgrayedd.py $h.f2b root@${h}:/root/bin/ipt
	ssh root@$h "apt-get install python3 -y"
	ssh root@$h "/root/bin/ipt/blocker.sh $h"
done
echo "Finished."

exit 0