#!/bin/bash

usage() {
    echo "Enter your SISTA account username for Dalek..."
    echo "e.g. ./get-users.sh aps"
}

#Bad arguments
if [ $# -eq 0 ]; then
        usage
        exit 0
fi

ssh ${1}@dalek "ypcat passwd | cut -f1 -d':' | sort -n" > ./sista-users.txt
