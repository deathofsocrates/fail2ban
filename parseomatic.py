"""
     AUTHOR:  Adam Stevens
       DATE:  11/06/12
DESCRIPTION:  This program parses a .mbox file and then creates output files for all fail2ban hack attempts    
"""

from operator import itemgetter
import itertools
import argparse
import sys
import re

host_names = ["tunnel1", "tunnel2", "rose", "martha", "doctor", "master", "presto", "to-vm", "yertle", "adama", "roslin", "vision", "river"]

class Host:

    def __init__(self, hostname):

        self.__ip = {}
        self.__hostname = hostname
        self.__attacks = 0
        self.__ports = []
        
    @property
    def hostname(self):
        return self.__hostname
        
    @hostname.setter
    def hostname(self, value):
        self.__hostname = value          
    
    @property
    def ip(self):
        return self.__ip
        
    @ip.setter
    def ip(self, value):
        self.__ip = value
        
    @property
    def attacks(self):
        return self.__attacks
        
    @attacks.setter
    def attacks(self, value):
        self.__attacks = value  
        
    @property
    def ports(self):
        return self.__ports
        
    @ports.setter
    def ports(self, value):
        self.__ports = value
        
class Ip:

    def __init__(self, ip):

        self.__ip = ip
        self.__country = ""
        self.__hostname = []
        self.__occurrences = 1
        self.__attacks = 0
        self.__ports = []
        
    @property
    def hostname(self):
        return self.__hostname
        
    @hostname.setter
    def hostname(self, value):
        self.__hostname = value    
        
    @property
    def country(self):
        return self.__country
        
    @country.setter
    def country(self, value):
        self.__country = value 
    
    @property
    def ip(self):
        return self.__ip
        
    @ip.setter
    def ip(self, value):
        self.__ip = value
        
    @property
    def occurrences(self):
        return self.__occurrences
        
    @occurrences.setter
    def occurrences(self, value):
        self.__occurrences = value
        
    @property
    def attacks(self):
        return self.__attacks
        
    @attacks.setter
    def attacks(self, value):
        self.__attacks = value  
        
    @property
    def ports(self):
        return self.__ports
        
    @ports.setter
    def ports(self, value):
        self.__ports = value
        
def ip_output(input_file, output_file):

    ip_dict = {}
    temp_hostname = ""           
    temp_ip = ""
    country = ""
    temp_attacks = 0
    threshold = 0       
    port_list = []
    start_attack = False
    
    global host_names
  
    f = open(input_file, 'r')
        
    for line in f:
    
        split_line = line.strip().split()
    
        if ((len(split_line) > 0) and (split_line[0] != "Fail2Ban")):
        
            if (split_line[0] == "Subject:"):
            
                if (re.match(r"^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", split_line[-1])):
                
                    temp_ip = split_line[-1]
                    
            elif (split_line[0].lower() == "country:"):

                country = split_line[1]
                
            elif (split_line[0] == "Regards,"):
            
                temp_attacks -= 1
                start_attack = False
                
            elif ((len(split_line) > 2) and (split_line[2] == "IP:" + temp_ip)):
            
                start_attack = True
                
            if (start_attack):
            
                temp_attacks += 1
                
                if ((len(split_line) >= 3) and (split_line[-3] == "port")):
                
                    if (split_line[-2].isdigit()):
                        port_list.append(split_line[-2])
                    
                    if split_line[3] == "to":
                        split_line[3] = split_line[3] + "-vm"
                    
                    if split_line[3] in host_names:
                        temp_hostname = split_line[3]
                    else:
                        temp_hostname = ""
                        temp_ip = ""
                                  
        elif (len(split_line)) == 1 and split_line[0] == "Fail2Ban":
        
            if (temp_ip != "" and (temp_hostname in host_names)):
            
                if (temp_ip not in ip_dict):
                        
                    temp_ip_object = Ip(temp_ip)
                    
                    ip_dict[temp_ip] = temp_ip_object
                                
                else:
                            
                    ip_dict[temp_ip].occurrences = (ip_dict[temp_ip].occurrences + 1)
                                
                ip_dict[temp_ip].attacks = (ip_dict[temp_ip].attacks + temp_attacks) 
                
                ip_dict[temp_ip].country = country

                if ((temp_hostname not in ip_dict[temp_ip].hostname)):
                
                    ip_dict[temp_ip].hostname.append(temp_hostname)                
                            
                for port in port_list:
                       
                    if port not in ip_dict[temp_ip].ports:
                       
                        ip_dict[temp_ip].ports.append(port)
                    
            
            temp_hostname = ""
                    
            temp_ip = ""
                
            temp_attacks = 0
                
            port_list = [] 
            
    o = open(output_file, 'w')
    
    for key in ip_dict:

        o.write("IP: " + ip_dict[key].ip + "\n")
        o.write("COUNTRY: " + ip_dict[key].country + "\n")
    
        o.write("HOSTNAME: ")
                
        ip_dict[key].hostname.sort(key = str)
         
        temp_string = ""
        for name in ip_dict[key].hostname:
            temp_string = temp_string + name + ","
        temp_string = temp_string[:-1]
        o.write(temp_string + "\n")
                 
        o.write("OCCURRENCES: " + str(ip_dict[key].occurrences) + "\n")
        o.write("ATTACKS: " + str(ip_dict[key].attacks) + "\n")
        o.write("PORTS: ")
                
        ip_dict[key].ports.sort(key = int)
            
        temp_string = ""
        for port in ip_dict[key].ports:
            port_list.append(port)
            temp_string = temp_string + port + ","
        temp_string = temp_string[:-1]
        o.write(temp_string + "\n\n")
        
        o.write("END\n\n")
        
    port_list.sort(key = int)
        
    o.write("PORT SPREAD: " + port_list[0] + " - " + port_list[-1])
                
    o.close()
    
def host_output(input_file, output_file):

    host_dict = {}
    temp_hostname = ""           
    temp_ip = ""
    temp_string = "" 
    temp_attacks = 0     
    port_list = []
    start_attack = False
    
    global host_names
  
    f = open(input_file, 'r')
        
    for line in f:
    
        split_line = line.strip().split()
    
        if ((len(split_line) > 0) and (split_line[0] != "Fail2Ban")):
        
            if (split_line[0] == "Subject:"):
            
                if (re.match(r"^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", split_line[-1])):
                
                    temp_ip = split_line[-1]
                
            elif (split_line[0] == "Regards,"):
            
                temp_attacks -= 1
                start_attack = False
                
            elif ((len(split_line) > 2) and (split_line[2] == "IP:" + temp_ip)):
            
                start_attack = True
                
            if (start_attack):
            
                temp_attacks += 1
                
                if ((len(split_line) >= 3) and (split_line[-3] == "port")):
                
                    if (split_line[-2].isdigit()):
                        port_list.append(split_line[-2])
                    
                    if split_line[3] == "to":
                        split_line[3] = split_line[3] + "-vm"
                    
                    if split_line[3] in host_names:
                        temp_hostname = split_line[3]
                    else:
                        temp_hostname = ""
                        temp_ip = ""
                
        elif (len(split_line)) == 1 and split_line[0] == "Fail2Ban":
        
            if (temp_ip != "" and (temp_hostname != "" and temp_hostname != "port")):
            
                if (temp_hostname not in host_dict):
                
                    temp_host_object = Host(temp_hostname)
                    
                    host_dict[temp_hostname] = temp_host_object
                                                            
                    host_dict[temp_hostname].ip[temp_ip] = 1
                    
                else:
                
                    if (temp_ip not in host_dict[temp_hostname].ip):
                    
                        host_dict[temp_hostname].ip[temp_ip] = 1
                        
                    else:
                    
                        host_dict[temp_hostname].ip[temp_ip] += 1
                                
                host_dict[temp_hostname].attacks = (host_dict[temp_hostname].attacks + temp_attacks)  
                            
                for port in port_list:
                       
                    if port not in host_dict[temp_hostname].ports:
                       
                        host_dict[temp_hostname].ports.append(port)
                    
            
            temp_hostname = ""
                
            temp_attacks = 0
                
            port_list = [] 

    o = open(output_file, 'w')
    
    for key in host_dict:

        o.write("HOSTNAME: " + host_dict[key].hostname + "\n")
    
        o.write("IPS: ")

        temp_list = list(host_dict[key].ip.keys())
        
        for i in range(len(temp_list)):
            temp_list[i] = "%3s.%3s.%3s.%3s" % tuple(temp_list[i].split("."))
            
        temp_list.sort()
        
        temp_list = list(temp_list for temp_list,_ in itertools.groupby(temp_list))
           
        temp_list = sorted(temp_list, key = itemgetter(0, 1, 2, 3))
        
        for i in range(len(temp_list)):
            temp_list[i] = temp_list[i].replace(" ", "")
         
        temp_string = ""
        for ip in temp_list:
            temp_string = temp_string + ip + ":" + str(host_dict[key].ip[ip]) + ","
        temp_string = temp_string[:-1]
        o.write(temp_string + "\n")
        
        o.write("ATTACKS: " + str(host_dict[key].attacks) + "\n")
        o.write("PORTS: ")

        host_dict[key].ports.sort(key = int)
            
        temp_string = ""
        for port in host_dict[key].ports:
            temp_string = temp_string + port + ","
        temp_string = temp_string[:-1]
        o.write(temp_string + "\n\n")
        
        o.write("END\n\n")
                
    o.close()            
    
def main():

    parser = argparse.ArgumentParser()
    
    parser.add_argument("input", type = str, help = "input mbox file")
    parser.add_argument("output", type = str, help = "creates host attack output file")
    parser.add_argument("-t", type = str, help = "creates ip attack output file")
    
    args = parser.parse_args()
    
    mbox_file = args.input
    
    if args.t:
        ip_output(mbox_file, args.t)
        
    host_output(mbox_file, args.output)
    
if __name__ == '__main__':
    main()
