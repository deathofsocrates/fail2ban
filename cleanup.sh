#!/bin/bash

echo "Removing old files..."

rm -rf all-hosts ipt iptables-rules tunnel1.f2b tunnel2.f2b takeout-201*.zip Takeout Fail2Ban.mbox

echo "Done... Exiting."

exit 0