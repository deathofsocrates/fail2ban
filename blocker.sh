#!/bin/bash

bin=/root/bin/ipt
fwdir=/etc/default
archive="${fwdir}/ipt-archive"
today=$(date +%Y.%m.%d.%H%M.%S)

usage() {
    echo "Enter the machine's hostname"
    echo "Exiting..."
}

#Bad arguments
if [ $# -eq 0 ]; then
        usage
        exit 0
fi

host=$1
echo $host
mkdir -p $archive
cp $fwdir/iptables-rules $archive/iptables-rules.$today

cp $fwdir/iptables-rules $bin/ipt

echo "Updating iptables rule-set..."
if [ $host == "tunnel1" ] || [ $host == "tunnel2" ]
then
		python3 $bin/upgrayedd.py $bin/${host}.f2b $bin/ipt  $fwdir/iptables-rules
else
	python3 $bin/upgrayedd.py $bin/${host}.f2b $bin/ipt  $fwdir/iptables-rules -p 22
fi
echo "Restarting iptables"
service iptables restart

exit 0