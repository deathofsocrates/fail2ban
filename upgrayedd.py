#!/usr/bin/python3

from operator import itemgetter
from copy import deepcopy
import collections
import itertools
import argparse
import sys

parser = argparse.ArgumentParser()
    
parser.add_argument("input_one", type = str, help = "input file to read, must be banhammer format")
parser.add_argument("input_two", type = str, help = "input file to read, must be ipt format")
parser.add_argument("output", type = str, help = "creates new ipt file in 'output' file")
parser.add_argument("-p", type = int, help = "creates rules for specific port number")
    
args = parser.parse_args()
    
input_file_one = args.input_one
input_file_two = args.input_two
output_file = args.output

ban_32 = []
ban_24 = []
ban_16 = []
ban_8 = []

def cidr_sort(passed_list):
    
    cidr_list = passed_list
    
    for i in range(len(cidr_list)):
        cidr_list[i] = "%3s.%3s.%3s.%3s" % tuple(cidr_list[i].split("."))
            
    cidr_list.sort()
        
    cidr_list = list(cidr_list for cidr_list,_ in itertools.groupby(cidr_list))
           
    cidr_list = sorted(cidr_list, key = itemgetter(0, 1, 2, 3))
        
    for i in range(len(cidr_list)):
        cidr_list[i] = cidr_list[i].replace(" ", "")
        
    return cidr_list

def condense():

    global ban_32
    global ban_24
    global ban_16
    global ban_8
    
    remove_32 = []
    remove_24 = []
    remove_16 = []
    for cidr in ban_8:
        temp_cidr = cidr.split(".")
        for ip in ban_32:
            temp_ip = ip.split(".")
            if temp_cidr[0] == temp_ip[0]:
                remove_32.append(ip)
                
        for ip in ban_24:
            temp_ip = ip.split(".")
            if temp_cidr[0] == temp_ip[0]:
                remove_24.append(ip)
                
        for ip in ban_16:
            temp_ip = ip.split(".")
            if temp_cidr[0] == temp_ip[0]:
                remove_16.append(ip)
                
    for cidr in ban_16:
        temp_cidr = cidr.split(".")
        for ip in ban_24:
            temp_ip = ip.split(".")
            if ((temp_cidr[0] == temp_ip[0]) and (temp_cidr[1] == temp_ip[1])):
                remove_32.append(ip)
                
        for ip in ban_24:
            temp_ip = ip.split(".")
            if ((temp_cidr[0] == temp_ip[0]) and (temp_cidr[1] == temp_ip[1])):
                remove_24.append(ip)
                
    for cidr in ban_24:
        temp_cidr = cidr.split(".")
        for ip in ban_24:
            temp_ip = ip.split(".")
            if ((temp_cidr[0] == temp_ip[0]) and (temp_cidr[1] == temp_ip[1]) and (temp_cidr[2] == temp_ip[2])):
                remove_32.append(ip)
  
    for cidr in remove_32:
        if cidr in ban_32:
            ban_32.remove(cidr)
        
    for cidr in remove_24:
        if cidr in ban_24:
            ban_24.remove(cidr)
        
    for cidr in remove_16:
        if cidr in ban_16:
            ban_16.remove(cidr)
        
    ban_32 = cidr_sort(ban_32)
    ban_24 = cidr_sort(ban_24)
    ban_16 = cidr_sort(ban_16)
    ban_8 = cidr_sort(ban_8) 
     
  
    
def select_list(ip):

    if ip[-3:] == "/32":
        if ip not in ban_32:
            ban_32.append(ip)
    elif ip[-3:] == "/24":
        if ip not in ban_24:
            ban_24.append(ip)
    elif ip[-3:] == "/16":
        if ip not in ban_16:
            ban_16.append(ip)
    elif ip[-2:] == "/8":
        if ip not in ban_8:
            ban_8.append(ip)
        
def main():
    
    i = open(input_file_one, 'r')
    
    for line in i:
        if line != "\n":
            split_line = line.split()
            ip = split_line[3]
            select_list(ip)
                    
    i.close()
    
    i = open(input_file_two, 'r')
    
    fail2ban_start = False
    fail2ban_end = False
    
    header = []
    footer = []
    
    for line in i:
        if line != "\n":
            if not fail2ban_start and not fail2ban_end:
                if line == "# Fail2Ban Drops\n":
                    fail2ban_start = True
                    header.append(line)
                else:
                    header.append(line)
            elif fail2ban_start and not fail2ban_end:
                if line == "# allow ssh from any\n":
                    fail2ban_end = True
                    fail2ban_start = False
                    footer.append(line)
                else:
                    split_line = line.split()
                    if "-p" in split_line:
                        select_list(split_line[-3])
                    else:
                        select_list(split_line[-3])
            elif fail2ban_end:
                footer.append(line)
                
    i.close()
    
    condense()
    
    o = open(output_file, 'w')
    
    for line in header:
        o.write(line)
        
    if args.p:   
        for ip in ban_8:
            o.write("-A INPUT -p tcp -m tcp --dport " + str(args.p) + " -s " + ip + " -j DROP\n")
            
        for ip in ban_16:
            o.write("-A INPUT -p tcp -m tcp --dport " + str(args.p) + " -s " + ip + " -j DROP\n")
            
        for ip in ban_24:
            o.write("-A INPUT -p tcp -m tcp --dport " + str(args.p) + " -s " + ip + " -j DROP\n")
            
        for ip in ban_32:
            o.write("-A INPUT -p tcp -m tcp --dport " + str(args.p) + " -s " + ip + " -j DROP\n")
    
    
    else:
        for ip in ban_8:
            o.write("-A INPUT -s " + ip + " -j DROP\n")
            
        for ip in ban_16:
            o.write("-A INPUT -s " + ip + " -j DROP\n")
            
        for ip in ban_24:
            o.write("-A INPUT -s " + ip + " -j DROP\n")
            
        for ip in ban_32:
            o.write("-A INPUT -s " + ip + " -j DROP\n")
        
    for line in footer:
        o.write(line)
        
    o.close()
        
if __name__ == '__main__':
    main()
                